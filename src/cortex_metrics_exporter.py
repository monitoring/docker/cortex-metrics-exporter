import os
import schedule
import boto3
import time
from prometheus_client import start_http_server, Gauge


def user_size(client, bucket, user_container, marker = None):
    size = 0
    if marker is None:
        objects = client.list_objects(Bucket=bucket, Prefix=user_container)
    else:
        objects = client.list_objects(Bucket=bucket, Prefix=user_container, Marker=marker)
    for object in objects["Contents"]:
        size += object["Size"]
    if objects["IsTruncated"]:
        next_marker = objects["NextMarker"]
        marker = objects["Marker"]
        rest_size = user_size(client, bucket, user_container, next_marker)
        return size + rest_size
    else:
        return size


def format_bytes(size):
    # 2**10 = 1024
    power = 1000
    n = 0
    power_labels = {0: "", 1: "k", 2: "M", 3: "G", 4: "T"}
    while size > power:
        size /= power
        n += 1
    return size, power_labels[n] + "B"


def get_s3_client_config_from_env():
    access_key_id = os.getenv("S3_ACCESS_KEY_ID")
    secret_access_key = os.getenv("S3_SECRET_ACCESS_KEY")
    endpoint_url = os.getenv("S3_ENDPOINT_URL")

    return {
        "aws_access_key_id": access_key_id,
        "aws_secret_access_key": secret_access_key,
        "endpoint_url": endpoint_url,
    }


def get_s3_bucket():
    return os.getenv("S3_BUCKET_NAME")


def request_users_usage():
    s3_config = get_s3_client_config_from_env()
    client = boto3.client("s3", **s3_config)

    bucket = get_s3_bucket()
    delim = "/"

    found_objects = client.list_objects(Bucket=bucket, Delimiter=delim)

    users = found_objects["CommonPrefixes"]
    user_prefixes = list(map(lambda x: x.get("Prefix"), users))

    user_sizes = {}

    for user_prefix in user_prefixes:
        s = user_size(client, bucket, user_prefix)
        user = os.path.dirname(user_prefix)
        user_sizes[user] = s
        print("[DEBUG] user %s, size %s", user_prefix, s)
    return user_sizes


def main():
    # Start the server to expose the metrics
    delay = int(os.getenv("S3_BUCKET_CHECK_DELAY"))
    start_http_server(8000)
    # Define the metrics to expose
    g0 = Gauge(
        "monit_cortex_S3_tenant_used_space", "Description of gauge", ["user", "bucket"]
    )
    g1 = Gauge(
        "monit_cortex_S3_tenant_used_space_last_check",
        "Last time when size was checked",
    )

    # Update the Gauges
    def update_metrics():
        try:
            usage = request_users_usage()
            g1.set_to_current_time()
            for user, space in usage.items():
                g0.labels(user, get_s3_bucket()).set(space)
        except Exception as e:
            print("[ERROR] Exception when updating metrics", e)

    update_metrics()
    # Set the metrics update to run every *delay* seconds
    schedule.every(delay).seconds.do(update_metrics)
    # Every second, check if there is a scheduled job to run
    while True:
        schedule.run_pending()
        time.sleep(1)


if __name__ == "__main__":
    main()
